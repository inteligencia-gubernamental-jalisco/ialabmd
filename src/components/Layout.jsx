import { Outlet } from 'react-router-dom'
import Footer from './footer/Footer'
import { Header } from './header/Header'
import { useIndex } from '../context/context';
import BackToTop from './navbar/BackToTop';

export default function Layout() {
	const { overflowRef } = useIndex()
	return (
		<div ref={overflowRef} id='infiniteScroll' className="flex flex-wrap flex-col min-h-screen justify-between bg-slate-100 dark:text-white dark:bg-[#0d1117]">
			<Header />

			<Outlet />

			<Footer />

			<BackToTop />

			<p className="hidden">Hello World!</p>
		</div>
	)
}
