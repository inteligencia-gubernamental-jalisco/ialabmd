import React from 'react'
import { Link } from 'react-router-dom'

function Logo() {
	return (
		<div className="flex items-center">
			<Link to='/'>
				<p className="text-2xl font-bold text-black dark:text-white ">IALAB</p>
			</Link>
		</div>
	)
}

export default Logo