import Events from "../home/Events"

function LeftSidebar() {

	return (
		<div className='hidden lg:block md:w-1/5 bg-white dark:bg-[#0d1117]'>
			<Events />
		</div>
	)
}

export default LeftSidebar
