import { regexMarkdownFileName } from "./text";

export function filterByMdFiles(fileList) {
	try {
		if (!Array.isArray(fileList)) throw new Error('No value in params.')
		return fileList.filter(item => 
			item.name?.match(regexMarkdownFileName)
		)
	}
	catch(error) {
		console.warn(error)
	}
}

export function getNestedHeadings(headingElements) {
	try {
		if (!Array.isArray(headingElements)) throw new Error('No list to sort')
		const nestedHeadings = [];
	
		headingElements.forEach((heading, index) => {
			const { innerText: title, id } = heading;
	
			if (heading.nodeName === "H2")
				nestedHeadings.push({ id, title, items: [] })
			else if (heading.nodeName === "H3" && nestedHeadings.length > 0)
				nestedHeadings[nestedHeadings.length - 1].items.push({ id, title })
		})
		return nestedHeadings
	} catch (error) {
		console.warn(error)
	}
}

export function sortByName(list) {
	try {
		if(!Array.isArray(list)) throw Error('No list to sort')
		const current = new Array(...list)

		return current.sort( (A,B) => {
			let a = A.name?.toLowerCase()
			let b = B.name?.toLowerCase()
			if(a < b) return -1
			if(a > b) return 1
			return 0
		})
	} catch (error) {
		console.warn(error)
	}
}

export function sortByDate(list) {
	try {
		if(!Array.isArray(list)) throw Error('No list to sort')
		const current = new Array(...list)
	
		return current.sort( (A,B) => {
			let a = A.date?.toLowerCase()
			let b = B.date?.toLowerCase()
			if(a < b) return 1
			if(a > b) return -1
			return 0
		})
	} catch (error) {
		console.warn(error)
	}
}
